# Olist Front End Test

> This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app)

## How to run

Install `create-react-app`

```bash
npm install -g create-react-app
```

```bash
git clone https://DawTaylor@bitbucket.org/DawTaylor/olist-test.git
cd olist-test
npm install
npm start
```
