import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'

import './index.css'

const Root = () => <App />

ReactDOM.render(<Root />, document.querySelector('#root'))
