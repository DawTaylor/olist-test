import React, { Component } from 'react'

import { AppWrapper } from './components/styled/App'
import { Box } from './components/styled/Box'

import { Logotype } from './components/Logotype'
import { Form } from './components/Form'

class App extends Component {
  render() {
    return (
      <AppWrapper>
        <Box>
          <div className='brand'>
            <Logotype />
            <p>Crie sua conta</p>
          </div>
          <Form />
        </Box>
      </AppWrapper>
    );
  }
}

export default App;
