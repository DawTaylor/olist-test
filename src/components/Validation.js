import React, { Component } from 'react'

import { ValidationWrapper, ValidationBar, ValidationBullets } from './styled/Validation'

export class Validation extends Component {  
  render() {
    const { validation } = this.props
    const bar1 = validation.errorCount === 2 ? 'danger' : validation.errorCount === 1 ? 'warning' : validation.errorCount === 0 ? 'success' : ''
    const bar2 = validation.errorCount === 1 ? 'warning' : validation.errorCount === 0 ? 'success' : ''
    const bar3 = validation.errorCount === 0 ? 'success' : ''
    return (
      <div className='formValidationWrapper'>
        <ValidationWrapper>
          <ValidationBar className={`bar ${bar1}`} />
          <ValidationBar className={`bar ${bar2}`} />
          <ValidationBar className={`bar ${bar3}`} />
        </ValidationWrapper>
        <ValidationBullets>
          <li className={validation.length === true ? 'danger' : validation.length === false ? 'success' : ''} >Pelo menos 6 caracteres</li>
          <li className={validation.uppercase === true ? 'danger' : validation.uppercase === false ? 'success' : ''}>Pelo menos 1 letra maiúscula</li>
          <li className={validation.digit === true ? 'danger' : validation.digit === false ? 'success' : ''}>Pelo menos 1 número</li>
        </ValidationBullets>
      </div>
    )
  }
}