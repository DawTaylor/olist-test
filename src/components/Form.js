import React, { Component } from 'react'

import { InputGroup } from './InputGroup'
import { Validation } from './Validation'

import { InputWrapper, InputButton } from './styled/Input'

export class Form extends Component {
  constructor(props) {
    super(props)

    this.updateField = this.updateField.bind(this)

    this.state = {
      name: null,
      email: null,
      password: null,
      'password_check': null,
      validation: {}
    }
  }

  componentWillUpdate(nextProps, nextState) {
    if(nextState.password !== null && this.state.password !== nextState.password) {
      const errors = this.validatePassword(nextState.password)
      this.setState({
        validation: {
          ...errors,
          errorCount: Object.keys(errors).map(key => !errors[key] ? false : true).reduce((acc, el) => el === true ? acc + 1 : acc, 0)
        }
      })
    }
  }

  validatePassword(password) {
    const upperPattern = /[A-Z]{1,}/g
    const digitPattern = /[\d]{1,}/g
    const lengthPattern = /[A-z\d]{6,}/g

    return {
      uppercase: !upperPattern.test(password),
      digit: !digitPattern.test(password),
      length: !lengthPattern.test(password)
    }
  }

  updateField(event) {
    this.setState({
      [event.target.name] : event.target.value
    })
  }

  render() {
    return (
      <div className='formWrapper'>
        <InputGroup
          inputId='name'
          inputLabel='Nome completo'
          inputPlaceholder='Digite seu nome'
          handler={this.updateField}
        />
        <InputGroup
          inputId='email'
          inputLabel='E-mail'
          inputPlaceholder='Digite seu e-mail'
          inputType='email'
          handler={this.updateField}
        />
        <InputGroup
          inputId='password'
          inputLabel='Senha'
          inputPlaceholder='Digite sua senha'
          inputType='password'
          handler={this.updateField}
          errorCount={this.state.validation.errorCount}
        />
        <Validation validation={this.state.validation} />
        <InputGroup
          inputId='passsord_check'
          inputLabel='Confirme sua senha'
          inputPlaceholder='Digite novamente sua senha'
          inputType='password'
          handler={this.updateField}
        />
        <InputWrapper>
          <InputButton>
            Criar conta
          </InputButton>
        </InputWrapper>
      </div>
    )
  }
}
