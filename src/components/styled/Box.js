import styled from 'styled-components'

export const Box = styled.div`
  background: #ffffff;
  padding: 60px 70px 48px;
  max-width: 515px;
  border: 3px solid #F2F2F2;

  @media screen and (max-width: 400px) {
    padding-right: 0;
    padding-left: 0;
    max-width: 100%;
    width: 375px;
  }

  .formWrapper {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }

  .formValidationWrapper {
    width: 100%;

    @media screen and (max-width: 400px) {
      width: 95%;
    }
  }
  
  .brand {
    text-align: center;
  }

  p {
    font-family: 'Open Sans', 'Helvetica Neue', Arial, sans;
    font-size: 22px;
    line-height: 31px;
    color: #312F4F;
    margin: 24px 0 13px;
  }
`
