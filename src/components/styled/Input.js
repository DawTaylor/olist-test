import styled from 'styled-components'

export const InputWrapper = styled.div`
  font-family: 'Open Sans', 'Helvetica', Arial, sans;
  color: #696D8C;
  display: flex;
  flex-direction: column;
  margin: 12px 0;
  width: 375px;

  @media screen and (max-width: 400px) {
    width: 95%;
  }
`

export const InputLabel = styled.label`
  font-size: 16px;
  line-height: 33px;
`

export const InputText = styled.input`
  width: 100%;
  padding: 15px;
  background: #FFFFFF;
  border: 1px solid #B6B9D0;
  box-sizing: border-box;
  box-shadow: inset 0px 3px 3px rgba(0, 0, 0, 0.05);
  font-size: 16px;
  font-family: 'Open Sans', Arial, sans;
  line-height: 26px;

  &::placeholder {
    color: #696D8C;
  }

  &.warning {
    border: 1px solid #F79682;
  }

  &.success {
    border: 1px solid #17D499;
  }
`

export const InputButton = styled.button`
  background: #17D499;
  padding: 10px 0;
  color: #ffffff;
  font-family: 'Open Sans', 'Helvetica Neue', Arial, sans;
  font-size: 18px;
  font-weight: bold;
  line-height: 30px;
  width: 100%;
  border: none;
`
