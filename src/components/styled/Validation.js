import styled from 'styled-components'

export const ValidationWrapper = styled.div`
  width: 100%;
  margin-top: 8px;
  display: flex;
  justify-content: space-between;
`

export const ValidationBar = styled.div`
  width: 119.67px;
  height: 8px;
  background: #EAEAF4;
  border-radius:10px;
  transition: all 0.35s;

  @media screen and (max-width: 400px) {
    width: 115px;
  }

  &.danger {
    background: #F79682;
  }

  &.warning {
    background: #F7BC1C;
  }

  &.success {
    background: #1FE6A8;
  }
`

export const ValidationBullets = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;
  li {
    font-family: 'Open Sans', 'Helvetica Neue', Arial, sans;
    font-size: 16px;
    line-height: 36px;
    display: flex;
    color: #696D8C;

    &:before {
      content: '•';
      font-size: 36pt;
      margin-right: 7px;
      color: #EAEAF4;
      transition: all 0.35s;
    }

    &.danger {
      &:before {
        color: #F79682;
      }
    }
    
    &.success {
      &:before {
        color: #1FE6A8;
      }
    }
    
  }
`
