import React, { Component } from 'react'

import logo from '../assets/img/logotype.svg'

export class Logotype extends Component {
  render() {
    return (
      <a href='/'>
        <img src={logo} alt='Olist Logo' />
      </a>
    )
  }
}
