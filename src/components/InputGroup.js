import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { InputWrapper, InputLabel, InputText } from './styled/Input'

export class InputGroup extends Component {
  render() {
    const { inputId, inputLabel, inputPlaceholder, inputType, errorCount } = this.props
    const feedbackClass = errorCount === 0 ? 'success' : errorCount > 0 ? 'warning' : ''
    return (
      <InputWrapper>
        <InputLabel htmlFor={inputId}>
          {inputLabel}
        </InputLabel>
        <InputText 
          id={inputId}
          type={inputType}
          placeholder={inputPlaceholder}
          name={inputId}
          onInput={this.props.handler}
          className={feedbackClass}
        />
      </InputWrapper>
    )
  }
}

InputGroup.propTypes = {
  inputId: PropTypes.string.isRequired,
  inputLabel: PropTypes.string.isRequired,
  inputPlaceholder: PropTypes.string,
  inputType: PropTypes.string
}

InputGroup.defaultProps = {
  inputType: 'text'
}
